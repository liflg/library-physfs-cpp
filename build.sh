#!/bin/bash

set -e

select_multiarchname()
{
    echo "Please select the desired architecture:"
    echo "1): i386-linux-gnu"
    echo "2): x86_64-linux-gnu"
    echo "3): armeabi-linux-android"
    echo "4): armeabi_v7a-linux-android"
    echo "5): i386-darwin-macos"
    echo "6): x86_64-darwin-macos"
    echo "7): universal-darwin-macos (fat binary i386 and x86_64)"
    read -r SELECTEDOPTION
    if [ x"$SELECTEDOPTION" = x"1" ]; then
        MULTIARCHNAME=i386-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"2" ]; then
        MULTIARCHNAME=x86_64-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"3" ]; then
        MULTIARCHNAME=armeabi-linux-android
    elif [ x"$SELECTEDOPTION" = x"4" ]; then
        MULTIARCHNAME=armeabi_v7a-linux-android
    elif [ x"$SELECTEDOPTION" = x"5" ]; then
        MULTIARCHNAME=i386-darwin-macos
    elif [ x"$SELECTEDOPTION" = x"6" ]; then
        MULTIARCHNAME=x86_64-darwin-macos
    elif [ x"$SELECTEDOPTION" = x"7" ]; then
        MULTIARCHNAME=universal-darwin-macos
    else
        echo "Invalid option selected!"
        select_multiarchname
    fi
}

linux_build()
{
    if [ x"$BUILDTYPE" = xdebug ]; then
        BUILDTYPE=Debug
    else
        BUILDTYPE=Release
    fi

    sed -i '/add_subdirectory(test)/d' source/CMakeLists.txt
    sed -i -e 's/add_library(physfs++ physfs.cpp)/add_library(physfs++ SHARED physfs.cpp)/' source/src/CMakeLists.txt

    mkdir "$BUILDDIR"
    rm -rf "$PREFIXDIR"

    ( PHYSFSDIR="${PWD}/../library-physfs/${MULTIARCHNAME}"
      cd "$BUILDDIR"
      cmake ../source \
        -DCMAKE_BUILD_TYPE=$BUILDTYPE \
        -DCMAKE_INSTALL_PREFIX="$PREFIXDIR" \
        -DCMAKE_CXX_FLAGS="-I${PHYSFSDIR}/include -L${PHYSFSDIR}/lib"
    make )

    mkdir -p "$PREFIXDIR"/include
    install -m 0644 source/include/physfs.hpp "$PREFIXDIR"/include

    mkdir -p "$PREFIXDIR"/lib
    install -m 0644 "$BUILDDIR"/src/libphysfs++.so "$PREFIXDIR"/lib

    # clean up afterwards
    ( cd source
      git clean -df .
      git checkout . )
}

if [ -z "$MULTIARCHNAME" ]; then
    echo "\$MULTIARCHNAME is not set!"
    select_multiarchname
fi

if [ -z "$BUILDDIR" ]; then
    BUILDDIR="build_$MULTIARCHNAME"
fi

if [ -z "$PREFIXDIR" ]; then
    PREFIXDIR="$PWD/$MULTIARCHNAME"
fi

case "$MULTIARCHNAME" in
i386-linux-gnu)
    linux_build;;
x86_64-linux-gnu)
    linux_build;;
*)
    echo "$MULTIARCHNAME is not (yet) supported by this script."
    exit 1;;
esac

install -m664 source/LICENSE.txt "$PREFIXDIR"/lib/LICENSE-physfs-cpp.txt

rm -rf "$BUILDDIR"

echo "PhysFS++ for $MULTIARCHNAME is ready."
