Website
=======
https://github.com/kahowell/physfs-cpp

License
=======
zlib license (see the file source/LICENSE.txt)

Version
=======
542470cb10cde5acb21cb906de25f8f4576c430a

Source
======
https://github.com/kahowell/physfs-cpp

Requires
========
* physfs
